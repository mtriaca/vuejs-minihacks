var ApiWrapper = {};

(function() {
	var conn

	ApiWrapper.setConnection = function(c) {
		conn = c
	}

	ApiWrapper.search = function(searchText, callback) {
		if(!searchText) {
			conn.query('SELECT Id, Name, Website, Type, Industry FROM Account LIMIT 1000').then(function(res) {
				callback(res.records, {status: true})
			}, function(err) {
				callback(null, {status: false, message: err.message})
			})
		} else {
			conn.search('FIND {' + searchText.replace(/([\{\}])/g, '\\$1') + '} IN Name Fields RETURNING Account(Id, Name, Website, Type, Industry)').then(function(res) {
				callback(res.searchRecords, {status: true})
			}, function(err) {
				callback(null, {status: false, message: err.message})
			})
		}
	}
})()